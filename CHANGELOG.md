# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.1

- patch: Updated contributing guidelines

## 0.2.0

- minor: Changed the docker image in build

## 0.1.0

- minor: Initial release
- patch: Fixed CI scripts

